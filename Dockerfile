FROM ruby:3.0.1

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1
# установим софт
RUN apt-get update && apt-get install -y postgresql-contrib libpq-dev vim

# установка гема Rails
RUN gem install rails

# подготовка к созданию пользователя
ARG USER=docker
ARG UID
ARG GID
ARG APP_DATABASE_PASSWORD
ENV APP_DATABASE_PASSWORD=${APP_DATABASE_PASSWORD}
# default password for user
ARG PW=docker

ARG APP_PATH=/home/docker/app

# add user
RUN useradd -m ${USER} --uid=${UID} && echo "${USER}:${PW}" | chpasswd

# настройка директории приложения
RUN mkdir ${APP_PATH}
COPY ./ ${APP_PATH}
RUN chown docker:docker -R ${APP_PATH}
WORKDIR ${APP_PATH}

RUN bundle config set --local path './vendor/bundler'

RUN chmod a+x ${APP_PATH}/docker-entrypoint.sh

USER ${UID}:${GID}

EXPOSE 3000
CMD [ "./docker-entrypoint.sh" ]