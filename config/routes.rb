Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      namespace :auth do
        post 'register', to: 'registration#register', as: 'registration'
        post 'signin', to: 'authentication#sign_in', as: 'sign_in'
        post 'refresh', to: 'authentication#refresh', as: 'refresh'
      end

      scope :users do
        get '/', to: 'users#index', as: 'users_index'
        get '/:id', to: 'users#show', as: 'users_show'
        patch '/:id', to: 'users#update', as: 'users_update'
        post '/:id/avatar/upload', to: 'users#upload_avatar', as: 'users_upload_avatar'
      end

      scope :posters do
        get '/', to: 'posters#index', as: 'posters_index'
        get '/:id', to: 'posters#show', as: 'posters_show'
      end
    end

    namespace :admin do
      namespace :auth do
        post 'signin', to: 'authentication#sign_in', as: 'sign_in'
      end

      scope :files do
        post 'upload', to: 'base#upload_file', as: 'upload_file'
        delete 'detach/:id', to: 'base#detach_file', as: 'detach_file'
      end

      scope :posters do
        get '/', to: 'posters#index', as: 'posters_index'
        get '/:id', to: 'posters#show', as: 'posters_show'
        post '/', to: 'posters#create', as: 'posters_create'
        patch '/:id', to: 'posters#update', as: 'posters_update'
        delete '/:id', to: 'posters#delete', as: 'posters_delete'
      end
    end
  end

end
