require "test_helper"

class Services::Files::FileServiceTest < ActiveSupport::TestCase
    test "save file via form" do
        temp_file = Rack::Test::UploadedFile.new(File.join(ActionDispatch::IntegrationTest.file_fixture_path, 'test_pic.png'), 'image/png')
        temp_file.original_filename
        file_model = Services::Files::FileService.save(temp_file)
        assert_instance_of(FileModel, file_model)
        assert file_model.namespace == 'general'
    end

    test "save file via json" do
        file_content = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAHCAYAAAA1WQxeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZSURBVBhXY/z58+d/BjyACUrjBENAAQMDAIW5A/gR2K9BAAAAAElFTkSuQmCC"
        file_model = Services::Files::FileService.save(file_content, 'general', 'test.png')
        assert_instance_of(FileModel, file_model)
    end
end