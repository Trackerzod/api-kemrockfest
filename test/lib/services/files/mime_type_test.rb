require "test_helper"

class Services::Files::MimeTypeTest < ActiveSupport::TestCase
    test "ext_by_mimetype" do
        ext = Services::Files::MimeType.ext_by_mimetype('audio/aac')
        assert_instance_of(String, ext)
        assert ext == 'aac'
    end

    test "ext_by_mimetype_with_wrong_mimetype" do
        ext = Services::Files::MimeType.ext_by_mimetype('test/test')
        assert_instance_of(String, ext)
        assert ext == 'txt'
    end

    test "ext_by_mimetype_with_double_mimetype" do
        ext = Services::Files::MimeType.ext_by_mimetype('text/html')
        assert_instance_of(String, ext)
        assert ext == 'html'
    end

    test "mimetype_by_ext" do
        mimetype = Services::Files::MimeType.mimetype_by_ext('aac')
        assert_instance_of(String, mimetype)
        assert mimetype == 'audio/aac'
    end

    test "mimetype_by_ext_with_wrong_ext" do
        mimetype = Services::Files::MimeType.mimetype_by_ext('test')
        assert_instance_of(String, mimetype)
        assert mimetype == 'text/plain'
    end

    test "is_image_with image mimetype" do
        assert Services::Files::MimeType.is_image?('image/png')
    end

    test "is_image_with not an image mimetype" do
        assert_not Services::Files::MimeType.is_image?('text/javascript')
    end
end