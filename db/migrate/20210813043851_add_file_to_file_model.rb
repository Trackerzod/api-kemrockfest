class AddFileToFileModel < ActiveRecord::Migration[6.1]
  def change
    add_column :file_models, :file, :string, null: false
  end
end
