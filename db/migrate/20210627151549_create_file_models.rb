class CreateFileModels < ActiveRecord::Migration[6.1]
  def change
    create_table :file_models do |t|
      t.string :name, null: false
      t.string :original_name
      t.string :full_path, null: false
      t.string :namespace, default: 'general'
      t.string :mime_type, null: false
      t.boolean :is_image, default: false
      t.references :fileable, polymorphic: true

      t.timestamps
    end
  end
end
