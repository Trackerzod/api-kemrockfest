class CreateAdminUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :admin_users do |t|
      t.string :login, null: false
      t.string :email, null: true
      t.string :password_digest

      t.timestamps
    end

    add_index :admin_users, :login, unique: true
    add_index :admin_users, :email, unique: true
  end
end
