class CreatePosters < ActiveRecord::Migration[6.1]
  def change
    create_table :posters do |t|
      t.string :name
      t.text :description
      t.boolean :active
      t.datetime :concert_date
      t.integer :price
      t.string :address
      t.string :place

      t.timestamps
    end
  end
end
