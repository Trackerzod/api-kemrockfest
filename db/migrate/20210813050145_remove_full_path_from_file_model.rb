class RemoveFullPathFromFileModel < ActiveRecord::Migration[6.1]
  def change
    remove_column :file_models, :full_path
  end
end
