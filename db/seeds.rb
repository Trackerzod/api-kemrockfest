# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Удаляем все что было
Admin::User.delete_all
Poster.delete_all
User.delete_all

# Создаём пользователя
10.times do |i|
    random = Random.new
    user = User.new(
        birthdate: Date.new(random.rand(1950..2000), random.rand(1..12), random.rand(1..28)),
        email: Faker::Internet.email,
        gender: random.rand(1..2),
        name: Faker::Name.first_name,
        surname: Faker::Name.last_name,
        patronymic: Faker::Name.middle_name,
        phone: "#{7999100}#{1234+i}"
    )
    user.password = '123456789'
    user.password_confirmation = '123456789'
    user.save
end

# Создаём афиши
20.times do |i|
    Poster.create(
        active: true,
        address: Faker::Address.street_address,
        concert_date: DateTime.now() + i.months,
        description: Faker::Lorem.paragraph,
        name: Faker::Music.band,
        place: ['бар "Ultimatum"', 'бар "Reload"'].sample,
        price: [0, 150, 200, 250].sample
    )
end

# Создаем админа
admin_password = '123'
admin = Admin::User.create(
    login: 'admin',
    email: 'admin@admin.com',
    password: admin_password,
    password_confirmation: admin_password
)
puts "Пароль администратора: #{admin_password}"