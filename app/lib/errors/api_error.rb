module Errors
    class ApiError
        attr_accessor :status, :title, :detail

        def initialize(status, title, detail)
            @status = status
            @title = title
            @detail = detail
        end
    end
end