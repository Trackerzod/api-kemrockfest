module Services
    class JsonWebToken
        SECRET_KEY = Rails.application.secret_key_base

        class << self
            # Создание токена пользователя
            #
            #   encode({ id: 1 })
            #   encode({ id: 1 }, 432455435324)
            #
            # @param payload [Hash] хэш информации о пользователе, как минимум должен содержать id
            # @param exp [Integer] время жизни токена в часах
            # @return [String], [Integer] токен и время жизни
            def encode(payload, exp = 24.hours.from_now)
                payload[:exp] = exp.to_i
                return JWT.encode(payload, SECRET_KEY, 'HS256'), payload[:exp]
            end
        
            # Декодирование токена в хэш с информацией
            #
            #   decode('eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MTEsImV4cCI6MTYyOTEwMDEzMX0.AF3TpBBTtr1-5CDJdZW7jgVY_Obn05Hr907VvtSYoec')
            #
            # @param token [String] токен пользователя из заголовка Authorization без приставки Bearer
            # @return [Hash]
            def decode(token)
                decoded = JWT.decode(token, SECRET_KEY, true, { algorithm: 'HS256' })[0]
                HashWithIndifferentAccess.new decoded
            end

            # Обновление утсаревшего токена
            #
            #   refresh('eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MTEsImV4cCI6MTYyOTEwMDEzMX0.AF3TpBBTtr1-5CDJdZW7jgVY_Obn05Hr907VvtSYoec')
            #
            # @param token [String] токен пользователя из заголовка Authorization без приставки Bearer
            # @return #see decode
            # @raise [Services::Exceptions::TokenException]
            def refresh(token)
                if in_blacklist?(token)
                    raise Exceptions::TokenException.new "Token in a blacklist."
                end
                blacklist(token)
                decoded = JWT.decode(token, SECRET_KEY, false, { algorithm: 'HS256' })[0];
                encode({id: decoded['id']})
            end

            private
            # Проверяет есть ли токен в черном списке
            #
            # @param [String] token
            # @return [Boolean]
            def in_blacklist?(token)
                redis = Redis.new(host: Rails.configuration.redis['host'], port: Rails.configuration.redis['port'])
                blacklisted_token = redis.get("blacklist_#{token}")
                return !blacklisted_token.nil?
            end
    
            # Отправляет токен в черный список
            # 
            #   blacklist('eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MTEsImV4cCI6MTYyOTEwMDEzMX0.AF3TpBBTtr1-5CDJdZW7jgVY_Obn05Hr907VvtSYoec')
            #
            # @param token [String] токен пользователя из заголовка Authorization без приставки Bearer
            def blacklist(token)
                redis = Redis.new(host: 'redis', port: 6379)
                redis.set("blacklist_#{token}", token)
            end
        end
    end
end