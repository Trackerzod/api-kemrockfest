require 'digest'
require 'base64'

module Services
    module Files
        class FileService
            class << self
                def save(file, namespace='general', filename=nil)
                    file_model = FileModel.new(namespace: namespace)

                    # проверяем строкой ли прислали файл
                    if file.is_a? String
                        # парсим строку на составляющие
                        parsed_file = convert_base64(file)

                        # проверяем наличие имени файла и генерируем новое имя если нет
                        filename = filename.nil? ? "#{Digest::Md5.new << Time.now().to_i.to_s}.#{parsed_file[:extension]}" : filename

                        # заполняем мета-данные в модель
                        file_model.mime_type = parsed_file[:content_type]
                        file_model.name = filename
                        file_model.original_name = filename
                        file_model.is_image = MimeType.is_image? parsed_file[:content_type]
                        
                        # создаем временный файл
                        temp_file_path = "/tmp/#{filename}"
                        tempfile = File.new(temp_file_path, 'w+')
                        tempfile.write(Base64.decode64(parsed_file[:content]).force_encoding('UTF-8'))
                        
                        # добавляем файл к модели
                        file_model.file = tempfile

                    else
                        # если не строка, то скорее всего это заргуженный файл через форму
                        file_model.mime_type = file.content_type
                        file_model.is_image = MimeType.is_image? file.content_type
                        file_model.name = file.original_filename
                        file_model.original_name = file.original_filename
                        file_model.file = file
                    end

                    file_model
                end

                # Конвертирует Base64-файл в хэш с расшифрованной информацией
                # 
                # @param base64_file [String] файл в формате Base64
                # @return [Hash]
                #   :content_type [String] MimeType файла
                #   :extension [String] Расширение файла
                #   :content [String] Содержимое файла в base64
                def convert_base64(base64_file)
                    parsed = Hash.new

                    parsed[:content_type] = base64_file[5, base64_file.index("\;")-5]
                    parsed[:extension] = MimeType.ext_by_mimetype(parsed[:content_type])
                    parsed[:content] = base64_file[base64_file.index("base64,")+7..]

                    parsed
                end
            end
        end
    end
end