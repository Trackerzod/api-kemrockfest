module Services
    module Files
        class MimeType
            TYPE_LIST = {
                'aac'  => 'audio/aac',
                'abw'  => 'application/x-abiword',
                'arc'  => 'application/x-freearc',
                'avi'  => 'video/x-msvideo',
                'azw'  => 'application/vnd.amazon.ebook',
                'bin'  => 'application/octet-stream',
                'bmp'  => 'image/bmp',
                'bz'   => 'application/x-bzip',
                'bz2'  => 'application/x-bzip2',
                'csh'  => 'application/x-csh',
                'css'  => 'text/css',
                'csv'  => 'text/csv',
                'doc'  => 'application/msword',
                'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'eot'  => 'application/vnd.ms-fontobject',
                'epub' => 'application/epub+zip',
                'gz'   => 'application/gzip',
                'gif'  => 'image/gif',
                'html' => 'text/html',
                'htm'  => 'text/html',
                'ico'  => 'image/vnd.microsoft.icon',
                'ics'  => 'text/calendar',
                'jar'  => 'application/java-archive',
                'jpg'  => 'image/jpeg',
                'jpeg' => 'image/jpeg',
                'js'   => 'text/javascript',
                'json' => 'application/json',
                'jsonld' => 'application/ld+json',
                'mid'  => 'audio/midi',
                'mjs'  => 'text/javascript',
                'mp3'  => 'audio/mpeg',
                'mpeg' => 'video/mpeg',
                'mpkg' => 'application/vnd.apple.installer+xml',
                'odp'  => 'application/vnd.oasis.opendocument.presentation',
                'ods'  => 'application/vnd.oasis.opendocument.spreadsheet',
                'odt'  => 'application/vnd.oasis.opendocument.text',
                'oga'  => 'audio/ogg',
                'ogv'  => 'video/ogg',
                'ogx'  => 'application/ogg',
                'opus' => 'audio/opus',
                'otf'  => 'font/otf',
                'png'  => 'image/png',
                'pdf'  => 'application/pdf',
                'php'  => 'application/php',
                'ppt'  => 'application/vnd.ms-powerpoint',
                'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                'rar'  => 'application/vnd.rar',
                'rtf'  => 'application/rtf',
                'sh'   => 'application/x-sh',
                'svg'  => 'image/svg+xml',
                'swf'  => 'application/x-shockwave-flash',
                'tar'  => 'application/x-tar',
                'tiff' => 'image/tiff',
                'tif'  => 'image/tiff',
                'ts'   => 'video/mp2t',
                'ttf'  => 'font/ttf',
                'txt'  => 'text/plain',
                'vsd'  => 'application/vnd.visio',
                'wav'  => 'audio/wav',
                'weba' => 'audio/webm',
                'webm' => 'video/webm',
                'webp' => 'image/webp',
                'woff' => 'font/woff',
                'woff2' => 'font/woff2',
                'xhtml' => 'application/xhtml+xml',
                'xls'  => 'application/vnd.ms-excel',
                'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'xml'  => 'application/xml',
                'xul'  => 'application/vnd.mozilla.xul+xml',
                'zip'  => 'application/zip',
                '3gp'  => 'video/3gpp',
                '3g2'  => 'video/3gpp2',
                '7z'   => 'application/x-7z-compressed'
            }

            IMAGE_MIMES = [
                'image/webp', 
                'image/png', 
                'image/jpeg',
                'image/gif', 
                'image/bmp',
                'image/svg+xml', 
                'image/vnd.microsoft.icon'
            ]

            class << self
                # Определяет расширение файла по его MIME TYPE
                #
                #   ext_by_mimetype('text/javascript') => 'js'
                #   ext_by_mimetype('image/png') => 'png'
                #
                # @param mimetype [String] MIME TYPE файла
                # @return [String]
                def ext_by_mimetype(mimetype)
                    mimetype = TYPE_LIST.select { |key, value| value == mimetype }
                    if mimetype.empty? 
                        return 'txt'
                    end

                    mimetype.keys.first.to_s
                end

                # Определяет MIME TYPE файла по его расширению
                #
                #   mimetype_by_ext('js') => 'text/javascript'
                #   mimetype_by_ext('png') => 'image/png'
                #
                # @param ext [String] расширение файла, предается без точки
                # @return [String]
                def mimetype_by_ext(ext)
                    TYPE_LIST.include?(ext) ? TYPE_LIST[ext] : 'text/plain'
                end

                # Определяет является ли mime-type файла картинкой.
                #
                #   is_image? 'image/png' => true
                #   is_image? 'text/html' => false
                #
                # @param mime_type [String] MIME-TYPE файла
                # @return [Boolean]
                def is_image?(mime_type)
                    IMAGE_MIMES.include? mime_type
                end
            end
        end
    end
end