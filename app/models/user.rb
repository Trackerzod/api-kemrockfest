# == Schema Information
#
# Table name: users
#
#  id              :bigint           not null, primary key
#  birthdate       :date
#  email           :string
#  gender          :integer
#  name            :string           not null
#  password_digest :string
#  patronymic      :string
#  phone           :string
#  surname         :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_users_on_email  (email) UNIQUE
#

class User < ApplicationRecord
    has_secure_password

    validates :email, uniqueness: true
    validates :name, presence: true

    has_many :files, as: :fileable, class_name: 'FileModel'
end
