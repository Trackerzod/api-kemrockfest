# == Schema Information
#
# Table name: admin_users
#
#  id              :bigint           not null, primary key
#  email           :string
#  login           :string           not null
#  password_digest :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_admin_users_on_email  (email) UNIQUE
#  index_admin_users_on_login  (login) UNIQUE
#
class Admin::User < ApplicationRecord
  has_secure_password

  validates :login, presence: true, uniqueness: true
  validates :email, uniqueness: true
end
