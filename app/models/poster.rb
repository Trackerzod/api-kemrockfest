# == Schema Information
#
# Table name: posters
#
#  id           :bigint           not null, primary key
#  active       :boolean
#  address      :string
#  concert_date :datetime
#  description  :text
#  name         :string
#  place        :string
#  price        :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
class Poster < ApplicationRecord
    has_many :files, as: :fileable, class_name: 'FileModel'

    scope :api_default_feed, -> { includes(:files).where(active: true).where("concert_date >= :current_date", { current_date: Time.now() }).order(concert_date: :asc) }
    scope :admin_default_feed, -> { includes(:files).order(concert_date: :desc) }
end
