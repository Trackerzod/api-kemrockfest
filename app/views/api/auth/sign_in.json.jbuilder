json.data do
    json.token @token
    json.expired @expired
    json.token_type 'Bearer'
    json.id @user ? @user.id : nil
end