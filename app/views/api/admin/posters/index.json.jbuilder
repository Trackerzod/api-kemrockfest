json.meta do
    json.page do
        json.current_page @posters.current_page
        json.per_page @posters.limit_value
        json.from @posters.limit_value
        json.total Poster.count
    end
end
json.data do
    json.array! @posters do |poster|
        json.partial! 'poster', poster: poster
    end
end