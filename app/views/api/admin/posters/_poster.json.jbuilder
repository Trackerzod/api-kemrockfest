json.id poster.id
json.name poster.name
json.description poster.description
json.price poster.price
json.active poster.active
json.place poster.place
json.address poster.address
json.concert_date poster.concert_date
json.active poster.active
json.created_at poster.created_at
json.updated_at poster.updated_at
json.files do
    json.array! poster.files do |file|
        json.partial! 'api/files/file', file: file
    end
end