json.id user.id
json.name user.name
json.surname user.surname
json.patronymic user.patronymic
json.gender user.gender
if @current_user && @current_user.id == user.id
    json.email user.email
    json.phone user.phone
    json.birthdate user.birthdate
end
json.created_at user.created_at
json.updated_at user.updated_at
json.files do
    json.array! user.files do |file|
        json.partial! 'api/files/file', file: file
    end
end