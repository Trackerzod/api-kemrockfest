json.meta do
    json.page do
        json.current_page @users.current_page
        json.per_page @users.limit_value
        json.from @users.limit_value
        json.total User.count
    end
end
json.data do
    json.array! @users do |user|
        json.partial! 'user', user: user
    end
end