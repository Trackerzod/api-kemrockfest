class Api::ApiController < ApplicationController
    # Рендер вьюхи ошибок.
    # При рендере используется переменная @errors.
    def render_errors
        render 'api/v1/errors'
    end

    # Добавляет ошибку в стек ошибок для последующего вывода.
    # Никаких значений не возвращается.
    # Стек является переменной класса.
    #
    #   add_error(400, "Bad Request", "Поле 'имя' должно быть заполнено")
    #
    # @param status [Integer] Http-статус ошибки
    # @param title [String] Заголовок ошибки
    # @param detail [String] Детальное описание ошибки
    def add_error(status, title, detail)
        if @errors.nil?
            @errors = []
        end
        error = Errors::ApiError.new(status, title, detail)
        @errors.push(error)
    end

    # Проверяет, есть ли ошибки в стеке.
    #
    # @param status [Integer]
    # @param title [String]
    # @param detail [String]
    # @return [Boolean]
    def has_errors?
        !@errors.nil? && @errors.count > 0
    end

    protected
    # Аутентификация пользователя по Bearer-токену.
    # Возвращает объект пользоваетля, это может быть как Admin::User, так и User
    #   
    #   auth_user(User)
    #   auth_user(Admin::User)
    #
    # @param klass [Class] класс пользователя
    # @return [User] если был прередан класс User
    # @return [Admin::User] если был прередан класс Admin::User
    # @return [nil] если пользователь не был найден
    # @raise [JWT::ExpiredSignature] если время жизни токена пользователя истекло
    def auth_user(klass)
        @current_user = nil
        token = get_token()
        unless token.nil?
            begin
                user_id = Services::JsonWebToken.decode(token)
                @current_user = klass.find(user_id[:id])
            rescue JWT::ExpiredSignature
                add_error(401, 'Срок токена', 'Время жизни токена истекло')
                # render_errors()
            end
        end
        @current_user
    end

    # Получение Bearer-токена из заголовка "Authorization"
    # 
    # @return [String] если заголовок был передан
    # @return [nil] если заголовок не был передан
    def get_token
        token = request.headers[:Authorization]
        unless token.nil?
            token = token.delete_prefix('Bearer ')
        end
        token
    end

    # Базовый экшн загрузки файла для всех контроллеров
    def upload_file
        uploaded_file = params[:file]
        original_filename = params[:name]
        namespace = params[:type]
        @file_model = Services::Files::FileService.save(uploaded_file, namespace, original_filename)

        @file_model.save!

        render 'api/files/file'
    end

    # Базовый экшн удаления связи модели с файлом
    def detach_file
        @file_model = FileModel.find(params[:id])
        @parent = @file_model.fileable
        @parent.files.delete(@file_model)
    end
end