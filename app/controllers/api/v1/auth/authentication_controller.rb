class Api::V1::Auth::AuthenticationController < Api::ApiController
    include Services

    def sign_in
        @user = User.find_by_email(params[:email])
        if @user && @user.authenticate(params[:password])
            @token, @expired = JsonWebToken.encode({ id: @user.id })
            render 'api/auth/sign_in'
        else
            add_error(401, 'Ошибка аутентификации', 'Неправильно введён email или пароль')
            render_errors
        end
    end

    def refresh
        token = get_token()
        begin
            @token, @expired = JsonWebToken.refresh(token)
            render 'api/auth/sign_in'
        rescue => exception
            add_error(401, 'Ошибка токена', exception.message)
            render_errors()
        end
    end
end