class Api::V1::Auth::RegistrationController < Api::ApiController
    def register
        if validate_register()
            @user = User.new(user_params)
            @user.password = params[:password]
            @user.password_confirmation = params[:password]
            if @user.save
                @token, @expired = Services::JsonWebToken.encode({ id: user.id })
                return render 'api/auth/sign_in', status: :created
            else
                @user.errors.each do |error|
                    add_error(400, 'Ошибка валидации', "#{error.attribute.to_s} #{error.message}")
                end
            end
        end
        render_errors()
    end

    private
    def user_params
        params.permit(:email, :name, :surname, :patronymic, :gender, :birthdate)
    end

    def validate_register
        add_error(400, 'Ошибка валидации', 'Неверно указан email') unless params[:email].present? && params[:email].match(URI::MailTo::EMAIL_REGEXP)
        add_error(400, 'Ошибка валидации', 'Пароль должен содержать 6 и более символов') unless params[:password].present? && params[:password].length >= 6
        !has_errors?()
    end
end
