class Api::V1::PostersController < Api::ApiController
    def index
        page = params[:page]
        page_number = page && page[:number] ? page[:number] : 1
        per_page = page && page[:size] ? page[:size] : 10
        @posters = Poster.api_default_feed.page(page_number).per(per_page)

        render :index
    end

    def show
        @poster = Poster.find(params[:id])
        
        unless @poster
            add_error(404, 'Ресурс не найден', 'Афиша не найдена')
            render_errors()
        end

        render :show
    end
end