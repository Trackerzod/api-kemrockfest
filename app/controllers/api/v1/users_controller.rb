class Api::V1::UsersController < Api::ApiController
    before_action -> { auth_user(User) }

    def index
        page = params[:page]
        page_number = page && page[:number] ? page[:number] : 1
        per_page = page && page[:size] ? page[:size] : 10
        @users = User.page(page_number).per(per_page)

        render :index
    end

    def show
        user_id = params[:id]
        @user = User.find(user_id)
        
        if @user
            render :show
         else
            add_error(404, 'Не найдено', "Пользователь с id #{user_id} не найден")
            render_errors()
        end
    end

    def update
        authorize_user()
        if has_errors?()
            return render_errors()
        end

        @user = User.find(params[:id])

        unless @user
            add_error(404, 'Не найдено', 'Пользователь не найден')
            return render_errors()
        end

        @user.update(user_params())
        
        if params[:files]
            file_models = FileModel.where(id: params[:files])
            @user.files << file_models
            @user.save
        end
        
        return render :show
    end

    def upload_avatar
        upload_file()
    end

    private
    def user_params
        params.permit(:email, :birthdate, :gender, :name, :surname, :patronymic, :phone)
    end

    def authorize_user
        if @current_user && @current_user.id != params[:id].to_i
            add_error(401, "Не авторизоаванная попытка", "Попытка обновления другого профиля")
        end
    end
end
