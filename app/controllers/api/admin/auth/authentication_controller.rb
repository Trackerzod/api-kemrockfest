class Api::Admin::Auth::AuthenticationController < Api::ApiController
    include Services

    def sign_in
        @user = Admin::User.find_by_login(params[:login])
        
        if @user && @user.authenticate(params[:password])
            @token, @expired = JsonWebToken.encode({ id: @user.id })
            render 'api/auth/sign_in'
        else
            add_error(401, 'Ошибка аутентификации', 'Неправильный логин или пароль!')
            render_errors
        end
    end
end