class Api::Admin::BaseController < Api::ApiController
    before_action -> { auth_user(Admin::User) }

    # Загрузка и привязывание файла к модели
    def upload_file
        if @current_user && @current_user.instance_of?(Admin::User)
            super
        else
            add_error(401, 'Ошибка аутентификации', 'Вы не аутентифицированы')
            render_errors()
        end
    end

    # Удаление связи файла с моделью
    def detach_file
        if @current_user && @current_user.instance_of?(Admin::User)
            super
        else
            add_error(401, 'Ошибка аутентификации', 'Вы не аутентифицированы')
            render_errors()
        end
    end
end