class Api::Admin::PostersController < Api::Admin::BaseController
    def index
        page = params[:page]
        page_number = page && page[:number] ? page[:number] : 1
        per_page = page && page[:size] ? page[:size] : 10

        @posters = Poster.admin_default_feed.page(page_number).per(per_page)

        render :index
    end

    def show
        @poster = Poster.find(params[:id])
        
        unless @poster
            add_error(404, 'Ресурс не найден', 'Афиша не найдена')
            render_errors()
        end

        render :show
    end

    def create
        @poster = Poster.new(poster_params)
        associate_files()

        if @poster.save
            render :show
        else
            @poster.errors.each do |error|
                add_error(400, 'Ошибка валидации', "#{error.attribute.to_s} #{error.message}")
            end
            render_errors()
        end
    end

    def update
        @poster = Poster.find(params[:id])

        if @poster.update(poster_params)
            associate_files()
            @poster.save

            render :show
        else
            @poster.errors.each do |error|
                add_error(400, 'Ошибка валидации', "#{error.attribute.to_s} #{error.message}")
            end
            render_errors()
        end
    end

    def delete
        @poster = Poster.find(params[:id])
        @poster.destroy
    end

    private
    def poster_params
        params.permit(:active, :address, :concert_date, :description, :name, :place, :price)
    end

    # Находит файлы по ID и добавляет к связи к афише
    def associate_files
        image_ids = params[:files]
        file_models = FileModel.where(id: image_ids)
        @poster.files << file_models
    end
end