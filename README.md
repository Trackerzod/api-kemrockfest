# README

## Системные требования
* Docker
* Docker-compose
  
## Как развернуть
* Склонировать проект: `git clone git@bitbucket.org:Trackerzod/api-kemrockfest.git`
* Зайти в папку **api-kemrockfest**
* Создать .env-файл: `cp .env.example .env`. При необходимости, поменять пароль к базе (APP_DATABASE_PASSWORD), GID и UID на свои, если у вашего пользователя они не равны 1000.
* Запустить проект: `docker-compose up`
* Перейти по адресу: `localhost:3080`. Админер БД: `localhost:8080`